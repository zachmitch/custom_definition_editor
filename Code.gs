var accessAccountsAndProperties;


/*************************************
Name - On Install
Purpose - Executes on install of add-on, populating the add-on menu
Inputs - n/a
Outputs - add on menu
*************************************/


function onInstall(e) {
  onOpen(e);
}

/*************************************
Name - On Open
Purpose - Executes on open, populating the add-on menu
Inputs - n/a
Outputs - add on menu
*************************************/


function onOpen(e) {
  SpreadsheetApp.getUi()
      .createAddonMenu()
      .addItem('1. Populate Spreadsheet', 'populate')
      .addItem('2. Overwrite Custom Definitions', 'overwrite')
      .addToUi();

}


/*************************************
Name - On Edit
Purpose - Executes when the user edits something on page
Inputs - n/a
Outputs - Changes to the sheet
*************************************/

function onEdit(e){
  var editedCell = e.range;
  var col = editedCell.getColumn();
  var row = editedCell.getRow();


  //If user has chosen an account, dynamically edit the associated properties
  if(col == 8 && row == 2){
    populatePropertyList(e);
  }
  //Keep track of edited fields by turning them blue
  if (isEditable(row,col)){
    SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Custom Definitions').getRange(row,col).setBackground("#cfe2f3").setHorizontalAlignment("center");
  }


}


/*************************************
Name - Is editable field
Purpose - Determines if the field we are editing is editable
Inputs - cell row and col
Outputs - bool
*************************************/
function isEditable(row, col){
  return (row >= 7 && row <= 206) && ((col >= 2 && col <= 4)||(col >= 7 && col <= 10));
}

/*************************************
Name - Populate Property List
Purpose - Populates the property list when the account is changed
Inputs - n/a
Outputs - property list cell is changed
*************************************/
function populatePropertyList(e){
    var editedCell = e.range;
    accessAccountsAndProperties = JSON.parse(SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Custom Definitions').getRange(88,26).getValue());
    var chosenAcctNumber = "x" + (/\d*$/.exec(e.value)[0]).toString();
    var propStrings = [];
    for (var i = 0; i < accessAccountsAndProperties[chosenAcctNumber].properties.length; i++){
      var temp = accessAccountsAndProperties[chosenAcctNumber].properties[i];
      propStrings.push(temp.name + " - " + temp.id);
    }
    var propList = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Custom Definitions').getRange(3,8);
    propList.setValue(propStrings[0]);
    var propListRule = SpreadsheetApp.newDataValidation().requireValueInList(propStrings).setAllowInvalid(true);
    propList.setDataValidation(propListRule).setBackground("white");
    editedCell.setBackground("white");
}

/*************************************
Name - Open this template
Purpose - Opens an html file from template
Inputs - html file, js file
Outputs - includes file in html
*************************************/

function openThisTemplate(File) {
  return HtmlService.createHtmlOutputFromFile(File).getContent();
}


/*************************************
Name - Populate
Purpose - Driving function for Option 1 of the add-on menu.  Populates the sheet w/ ga data
Inputs - n/a
Outputs - sheet with infos
*************************************/

function populate(){

  var ss = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Custom Definitions');

  if (ss != null) {
    ss = SpreadsheetApp.getActive();
    var sheet = ss.getSheetByName('Custom Definitions');
    ss.deleteSheet(sheet);
  }

  SpreadsheetApp.getActiveSpreadsheet().insertSheet('Custom Definitions');

  var chooseAccount = HtmlService.createTemplateFromFile('chooseAccount').evaluate().setWidth(400).setHeight(200);
  SpreadsheetApp.getUi().showModalDialog(chooseAccount, "Choose GA Account and Property to Edit/Copy");


}

/*************************************
Name - overwrite
Purpose - Driving function for Option 2 of the add-on menu.  Overwrites ga acct with sheet data
Inputs - sheet
Outputs - overwritten ga acct
*************************************/

function overwrite(){



  // grab current in sheet
  var sheetDefinitions = getSheetCustomDefs();

  // grab current in ga
  var gaDefinitions = getCustomDefinitions(sheetDefinitions.account,sheetDefinitions.property);

  // compare ga and ss
  var definitionChanges = compareDefinitions(gaDefinitions, sheetDefinitions);

  // alert that stuff is going to change forever
  var proceedWithEdits = alertOverwrite(sheetDefinitions.account, sheetDefinitions.property);

  // make updates
  if(proceedWithEdits){
    //email keeping track of changes
    sheetDefinitions.emailChanges = "Changes made to GA Account: " + sheetDefinitions.account + ", Property: " + sheetDefinitions.property + "\n********************************************\n\n";
    makeEdits(definitionChanges, sheetDefinitions);
    sendEmail(sheetDefinitions.emailChanges, sheetDefinitions.account,sheetDefinitions.property);
    alertFinished();
  }


}

//**********************************
// Name: Alert that they are about to overwrite
// Purpose:  Tell the user they're about to overwrite CDs in GA account and they can't undo it
// Inputs: n/a
// Outputs: boolean
//**********************************/
  function alertOverwrite(acct,prop) {
    var ui = SpreadsheetApp.getUi()
    var result = ui.alert(
     'Be careful. This cannot be undone.',
      'You are about to overwrite custom definitions in Google Analytics for:\n\nAccount: ' + acct+ '\nProperty: ' + prop + '\n\nAre you sure you want to continue?',
      ui.ButtonSet.YES_NO
    );

    //If user selects yes
    return result == ui.Button.YES;
  }


  //**********************************
// Name: Alert finished
// Purpose:  Tell the user all changes have taken place
// Inputs: n/a
// Outputs: boolean
//**********************************/
  function alertFinished() {
    var ui = SpreadsheetApp.getUi()
    var result = ui.alert(
     'Changes pushed to GA',
      'You will receive an email documenting these changes at:\n\n' + Session.getActiveUser().getEmail() + '.' ,
      ui.ButtonSet.OK
    );
  }

//**********************************
// Name: Make edits
// Purpose:  Makes edits to the target ga acct/property
// Inputs: Object holding edits, email string to document changes
// Outputs: n/a
//**********************************/
  function makeEdits(changesObject, emailChanges) {
    //Edit dimensions
    if(changesObject.customDimensions.length > 0) {
      for (var i = 0; i < changesObject.customDimensions.length; i++){

         try{
            if(changesObject.customDimensions[i].id == "newDefinition"){

              Analytics.Management.CustomDimensions.insert(
                changesObject.customDimensions[i],
                changesObject.account,
                changesObject.property
              );
              Logger.log(JSON.stringify(changesObject.customDimensions[i]));
              writeEmail(emailChanges, changesObject.customDimensions[i].id + " - INSERTED - " + changesObject.customDimensions[i].name);
            } else {
              Analytics.Management.CustomDimensions.update(
                changesObject.customDimensions[i],
                changesObject.account,
                changesObject.property,
                changesObject.customDimensions[i].id
              );
              writeEmail(emailChanges, changesObject.customDimensions[i].id + " - EDITED - " + changesObject.customDimensions[i].name);
            }

            } catch(e) {
              SpreadsheetApp.getUi().alert('Check something real quick','Make sure you filled out name, scope, and active for all edited Custom Dimensions.', SpreadsheetApp.getUi().ButtonSet.OK);
           }

        Utilities.sleep(350);
      }
    }

    //Edit metrics
    if(changesObject.customMetrics.length > 0) {
       for (var i = 0; i < changesObject.customMetrics.length; i++){
        try{
             if(changesObject.customMetrics[i].id == "newDefinition"){

               delete(changesObject.customMetrics[i].id);
               Analytics.Management.CustomMetrics.insert(
                changesObject.customMetrics[i],
                changesObject.account,
                changesObject.property
              );
              writeEmail(emailChanges, changesObject.customMetrics[i].id + " - INSERTED - " + changesObject.customMetrics[i].name);

             } else {
                Logger.log("~ " + changesObject.customMetrics[i].id);
               Analytics.Management.CustomMetrics.update(
                changesObject.customMetrics[i],
                changesObject.account,
                changesObject.property,
                changesObject.customMetrics[i].id
              );
              writeEmail(emailChanges, changesObject.customMetrics[i].id + " - EDITED - " + changesObject.customMetrics[i].name);


             }
       } catch(e) {

             SpreadsheetApp.getUi().alert('Check something real quick','Make sure you filled out name, format, scope, and active for all edited Custom Metrics.', SpreadsheetApp.getUi().ButtonSet.OK);

       }
         Utilities.sleep(350);
      }
    }
  }

/*************************************
Name - get custom defs from sheet
Purpose - Driving function for Option 2 of the add-on menu.  Overwrites ga acct with sheet data
Inputs - sheet
Outputs - overwritten ga acct
*************************************/

function getSheetCustomDefs(){
  var sheetCustomDefs = {};
  var accountAndProp = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Custom Definitions').getRange(2,1,205,11).getDisplayValues();

  try {

    sheetCustomDefs['account'] = (/\d*$/.exec(accountAndProp[0][7])[0]);
    sheetCustomDefs['property'] =(/UA-\d*-\d*$/.exec(accountAndProp[1][7])[0]);
    sheetCustomDefs['customDimensions'] = {};
    sheetCustomDefs['customDimensions']['items'] = [];
    sheetCustomDefs['customMetrics'] = {};
    sheetCustomDefs['customMetrics']['items'] = [];

    for (var i = 0; i < 200; i++){
      var tempCD =  {};
      tempCD['id'] = "ga:dimension"+(/\d*$/.exec(accountAndProp[5+i][0]));
      tempCD['name'] = accountAndProp[5+i][1];
      tempCD['scope'] = accountAndProp[5+i][2];
      tempCD['active'] = (accountAndProp[5+i][3] == "TRUE");
      if(tempCD['name'].trim() == ""){
        break;
      }
      sheetCustomDefs.customDimensions.items.push(tempCD);
    }

    for (var i = 0; i < 200; i++){
      var tempCM =  {};
      tempCM['id'] = "ga:metric"+(/\d*$/.exec(accountAndProp[5+i][5]));
      tempCM['name'] = accountAndProp[5+i][6];
      tempCM['type'] = accountAndProp[5+i][7];
      tempCM['scope'] = accountAndProp[5+i][8];
      tempCM['active'] = (accountAndProp[5+i][9] == "TRUE");
      if(tempCM['name'].trim() == ""){
        break;
      }
      sheetCustomDefs.customMetrics.items.push(tempCM);
    }

    return sheetCustomDefs;

  } catch (e) {

      SpreadsheetApp.getUi().alert(
      'There was an error grabbing information from the Google Sheet',
      'Be sure you chose a destination account and property.',
      SpreadsheetApp.getUi().ButtonSet.OK
      );

  }

}

//TODO
/*************************************
Name - collect changes
Purpose - Compares the custom definitions in ga to the ones on the sheet, noting the differences
Inputs - sheet and ga data
Outputs - n/a
*************************************/

function collectChanges(gaData, sheetData){
  if(gaData.account != sheetData.account || gaData.property != sheetData.property){
    throw new error();
  }
  var accountChangesObject = {};
  accountChangesObject['account'] = gaData.account;
  accountChangesObject['property'] = gaData.property;
}

/*************************************
Name - compare definitions
Purpose - compares two definitions objects (dimension/metric)
Inputs - definition 1, definition 2
Outputs - boolean
*************************************/

function compareDefinitions(gaDefs, sheetDefs){
  //avoid catastrophic overwrite of the wrong account
  if(gaDefs.account !== sheetDefs.account || gaDefs.property !== sheetDefs.property){
    throw "Strangely, these accounts/properties do not match.  Abort.";
  }

  //Object to hold the changes
  var definitionChanges = {};
  definitionChanges['account'] = gaDefs.account;
  definitionChanges['property'] = gaDefs.property;
  definitionChanges['customDimensions'] = [];
  definitionChanges['customMetrics'] = [];
  definitionChanges['previousDimLength'] = gaDefs.customDimensions.items.length;
  definitionChanges['previousMetLength'] = gaDefs.customMetrics.items.length;

  //See if we are adding any additional dimensions or metrics that aren't currently in GA
  var addDimensions = sheetDefs.customDimensions.items.length > definitionChanges['previousDimLength'];
  var addMetrics = sheetDefs.customMetrics.items.length > definitionChanges['previousMetLength'];

  //Check dimensions
  for (var i = 0; i < gaDefs.customDimensions.items.length; i++){
    if( sheetDefs.customDimensions.items[i].name != gaDefs.customDimensions.items[i].name ||
        sheetDefs.customDimensions.items[i].active != gaDefs.customDimensions.items[i].active ||
        sheetDefs.customDimensions.items[i].scope != gaDefs.customDimensions.items[i].scope){
      //push to changes
      definitionChanges.customDimensions.push(sheetDefs.customDimensions.items[i]);
    }
  }
  //If we are adding new dimensions to our GA give them a special ID to handle them differently
  if(addDimensions){
    for (var i = definitionChanges['previousDimLength']; i < sheetDefs.customDimensions.items.length; i++){
      sheetDefs.customDimensions.items[i].id = "newDefinition";
      definitionChanges.customDimensions.push(sheetDefs.customDimensions.items[i]);
    }
  }

  //Check metrics
  for (var i = 0; i < gaDefs.customMetrics.items.length; i++){
    if( sheetDefs.customMetrics.items[i].name != gaDefs.customMetrics.items[i].name ||
        sheetDefs.customMetrics.items[i].active != gaDefs.customMetrics.items[i].active ||
        sheetDefs.customMetrics.items[i].scope != gaDefs.customMetrics.items[i].scope ||
        sheetDefs.customMetrics.items[i].type != gaDefs.customMetrics.items[i].type){
      //push to changes
      definitionChanges.customMetrics.push(sheetDefs.customMetrics.items[i]);
    }
  }
  //If we are adding new metrics to our GA
  if(addMetrics){
    for (var i = definitionChanges['previousMetLength']; i < sheetDefs.customMetrics.items.length; i++){
      sheetDefs.customMetrics.items[i].id = "newDefinition";
      definitionChanges.customMetrics.push(sheetDefs.customMetrics.items[i]);
    }
  }


  Logger.log(definitionChanges);

  return definitionChanges;
}

/*************************************
Name - Write email
Purpose - appends content to email
Inputs - email string, additional string
Outputs - n/a
*************************************/

function writeEmail(sheetObject, additionalString){
   sheetObject.emailChanges += additionalString + '\n';
}

/*************************************
Name - Send email
Purpose - sends emailstring to user using the application
Inputs - email string
Outputs - n/a
*************************************/

   function sendEmail(emailString, accountChanged, propertyChanged){
     var email = Session.getActiveUser().getEmail();
     var subject = "Changes made to GA Account: " + accountChanged + ", Property: " + propertyChanged;
     var message = emailString;
     MailApp.sendEmail(email, subject, message);
   }


/*************************************
Name - Get Accounts
Purpose - Returns accounts and properties for a GA user
Inputs - n/a
Outputs - GA accounts/properties
*************************************/

function getAccounts(){
  return  Analytics.Management.AccountSummaries.list({
   fields: 'items(id, name, webProperties( id, name ))'
  });
}


/*************************************
Name - Get Custom Definitions
Purpose - Gets custom dimensions and metrics for GA acct/prop
Inputs - GA account/property
Outputs - object that contains:  acct, property, cds, cms
*************************************/

function getCustomDefinitions(account,property){

  var customDefinitions = {};
  customDefinitions['account'] = account;
  customDefinitions['property'] = property;
  customDefinitions['customDimensions'] = Analytics.Management.CustomDimensions.list(account, property, { fields: 'items(id, name, index, scope, active, updated)'});
  customDefinitions['customMetrics'] = Analytics.Management.CustomMetrics.list(account, property, { fields: 'items(id, name, index, scope, type, active, updated)'});
  //Logger.log(customDefinitions);
  return customDefinitions;

}

/*************************************
Name - Populate Spreadsheet
Purpose - Fills out spread sheet with GA data
Inputs - Custom definition object
Outputs - Filled out sheet
*************************************/

function populateSpreadsheet(cdObject, dropdownObject){
    var ss = SpreadsheetApp.getActiveSpreadsheet().getSheetByName('Custom Definitions') ||
    SpreadsheetApp.getActiveSpreadsheet().insertSheet('Custom Definitions');


    //Can't easily pass object from client side JS to server side functions, so I save it in cell Z88
    var sndStrng = JSON.stringify(dropdownObject);
    var dropDownCell = ss.getRange(88,26)
    .setValue(sndStrng);
    accessAccountsAndProperties = dropDownCell.value;

    // Setup Adswerve logo marquee
    ss.setRowHeight(1, 120);
    var logoMasthead = ss.getRange(1,1,1,11)
      .merge()
      .setHorizontalAlignment("center")
      .setFormula('=IMAGE("https://storage.googleapis.com/adswerve_logo/as_logo.png")');

    var srcGA = ss.getRange(2,1,2,1)
      .merge()
      .setBackground("black")
      .setFontWeight("bold")
      .setFontColor("white")
      .setFontSize(12)
      .setValue("Source")
      .setHorizontalAlignment("center")
      .setVerticalAlignment("middle");

    var destGA = ss.getRange(2,6,2,1)
      .merge()
      .setBackground("black")
      .setFontColor("white")
      .setFontWeight("bold")
      .setFontSize(12)
      .setValue("Destination")
      .setHorizontalAlignment("center")
      .setVerticalAlignment("middle");


    var srcAcct = ss.getRange(2,2,1,4)
      .merge()
      .setValue("Account - " + dropdownObject.chosenAccount)
      .setFontWeight("bold")
      .setBackground("#d9d9d9");

    var srcProp = ss.getRange(3,2,1,4)
      .merge()
      .setValue("Property - " + dropdownObject.chosenProperty)
      .setFontWeight("bold")
      .setBackground("#d9d9d9");


    var dstAcct = ss.getRange(2,7,1,1)
      .setValue("Account")
      .setFontWeight("bold")
      .setHorizontalAlignment("center");



    var fullAcctArray = ["Choose Destination Account and Property"];
    for (var prp in dropdownObject){
      if(dropdownObject[prp].name != undefined){
        fullAcctArray.push(dropdownObject[prp].name);
      }
    }

    Logger.log(fullAcctArray);
    var acctScopeRule = SpreadsheetApp.newDataValidation().requireValueInList(fullAcctArray).setAllowInvalid(true);

    var dstAcctName = ss.getRange(2,8,1,4)
      .merge()
      .setBackground("#ea9999")
      .setDataValidation(acctScopeRule)
      .setValue("Choose Destination Account and Property");


    var dstProp = ss.getRange(3,7,1,1)
      .setValue("Property")
      .setFontWeight("bold")
      .setHorizontalAlignment("center");

    var dstPropName = ss.getRange(3,8,1,4)
      .merge()
      .setBackground("#ea9999");

    var separator = ss.getRange(4,1,1,11)
      .merge()
      .setHorizontalAlignment("center")
      .setFontWeight("bold")
      .setFontSize(12)
      .setFontColor("white")
      .setBackground("black");
      //.setValue("Make edits to the Destination GA Account/Property below")


    var cdHeader = ss.getRange(5,1,1,5)
      .merge()
      .setValue("Custom Dimensions")
      .setHorizontalAlignment("center")
      .setFontWeight("bold")
      .setFontSize(14)
      .setBackground("#08335d")
      .setFontColor("white");

    var cmHeader = ss.getRange(5,6,1,6)
      .merge()
      .setValue("Custom Metrics")
      .setHorizontalAlignment("center")
      .setFontWeight("bold")
      .setFontSize(14)
      .setBackground("#5b7e95")
      .setFontColor("white");

    var columnHeads = [["INDEX","NAME","SCOPE","ACTIVE?","LAST CHANGE", "INDEX", "NAME", "FORMAT", "SCOPE", "ACTIVE?", "LAST CHANGE"]];
    ss.getRange(6,1,1,11).setValues(columnHeads).setFontWeight("bold").setHorizontalAlignment("center").setFontColor("white").setBackground("#5b7e95");
    ss.getRange(6,2,1,4).setBackground("#08335d");

    //indices
    ss.getRange(6, 1, 1).setBackground("#08335d").setFontColor("white").setHorizontalAlignment("center");
    ss.getRange(6, 6, 1).setBackground("#5b7e95").setFontColor("white").setHorizontalAlignment("center");

    for (var i = 0; i < 200; i++){
      ss.getRange(7 + i, 1, 1).setValue("CD"+(i+1)).setBackground("#08335d").setFontColor("white").setHorizontalAlignment("center");
      ss.getRange(7 + i, 6, 1).setValue("CM"+(i+1)).setBackground("#5b7e95").setFontColor("white").setHorizontalAlignment("center");
      ss.getRange(7+i, 5, 1).setBackground("#d9d9d9");
      ss.getRange(7+i, 11, 1).setBackground("#d9d9d9");
    }

    //scopes
  //TODO remove allow invalid
    var CDscopesPossible = ["HIT", "SESSION", "USER", "PRODUCT"];
    var CMscopesPossible = ["HIT", "PRODUCT"];
    var CMtypes = ["INTEGER","CURRENCY","TIME"];
    var CDscopeRule = SpreadsheetApp.newDataValidation().requireValueInList(CDscopesPossible).setAllowInvalid(true);
    var CMscopeRule = SpreadsheetApp.newDataValidation().requireValueInList(CMscopesPossible).setAllowInvalid(true);
    var CMtypesRule = SpreadsheetApp.newDataValidation().requireValueInList(CMtypes).setAllowInvalid(true);
    var cdScope = ss.getRange(7,3,200);
    var cmScope = ss.getRange(7,9,200);
    var cmType = ss.getRange(7,8,200);
    cdScope.setDataValidation(CDscopeRule);
    cmScope.setDataValidation(CMscopeRule);
    cmType.setDataValidation(CMtypesRule);

    // active
   //TODO remove allow invalid
    var activePossible = ["TRUE", "FALSE"];
    var activeRule = SpreadsheetApp.newDataValidation().requireValueInList(activePossible).setAllowInvalid(true);
    var cdActive = ss.getRange(7,4,200);
    var cmActive = ss.getRange(7,10,200);
    cdActive.setDataValidation(activeRule);
    cmActive.setDataValidation(activeRule);

    //dimensions
    for (var i = 0; i < cdObject.customDimensions.items.length; i++){
      ss.getRange(7 + i, 2, 1).setValue(cdObject.customDimensions.items[i].name).setHorizontalAlignment("center");
      ss.getRange(7 + i, 3, 1).setValue(cdObject.customDimensions.items[i].scope).setHorizontalAlignment("center");
      ss.getRange(7 + i, 4, 1).setValue(cdObject.customDimensions.items[i].active).setHorizontalAlignment("center");
      ss.getRange(7 + i, 5, 1).setValue(cdObject.customDimensions.items[i].updated.split("T")[0]).setHorizontalAlignment("center");
    }

    // metrics
    for (var i = 0; i < cdObject.customMetrics.items.length; i++){
      ss.getRange(7 + i, 7, 1).setValue(cdObject.customMetrics.items[i].name).setHorizontalAlignment("center");
      ss.getRange(7 + i, 8, 1).setValue(cdObject.customMetrics.items[i].type).setHorizontalAlignment("center");
      ss.getRange(7 + i, 9, 1).setValue(cdObject.customMetrics.items[i].scope).setHorizontalAlignment("center");
      ss.getRange(7 + i, 10, 1).setValue(cdObject.customMetrics.items[i].active).setHorizontalAlignment("center");
      ss.getRange(7 + i, 11, 1).setValue(cdObject.customMetrics.items[i].updated.split("T")[0]).setHorizontalAlignment("center");
    }

    function onEdit(e){

      Logger.log(e.range);
    }
}
