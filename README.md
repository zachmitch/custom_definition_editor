# Google Analytics Custom Definition Editor

This is a Google Sheets add-on that allows an Adswerve user to edit or copy custom dimensions and metrics of a Google Analytics property they have access to.

[This tool was developed for/using Apps Script](https://script.google.com/a/adswerve.com/d/1qnJBecvkSlcIm4eEAt49FRZBtCJ_Q9gDl02qk_AdSZZkGRgo443tlRvf/edit?usp=sharing).

[It has its own Google Cloud Project associated with it](https://console.cloud.google.com/home/dashboard?project=custom-definition-manager), project number: 940767061127.

## Getting the Tool

Note that this is only available to Adswerve users.

The Custom Definition Editor can be accessed [here](https://gsuite.google.com/marketplace/app/ga_custom_definition_manager/940767061127?hl=en&pann=sheets_addon_widget).


## Using the Tool

### Editing a GA Property

```
1.  Open a new Google Sheets spreadsheet
2.  Get current GA property data:  Add ons > GA Custom Definition Editor > Populate Spreadsheet
3.  Choose a destination property to send your edits to (red boxes)
4.  Make edits
5.  When ready to overwrite:  Add ons > GA Custom Definition Editor > Overwrite Custom Defintions
```

### Copying from GA Property A >> GA Property B

```
1.  Open a new Google Sheets spreadsheet
2.  Get current GA property data:  Add ons > GA Custom Definition Editor > Populate Spreadsheet
3.  Choose a destination property to copy to (red boxes)
4.  Copy:  Add ons > GA Custom Definition Editor > Overwrite Custom Defintions
```

## Improvements needed
* Select which CD's / CM's will be copied/edited -- currently all on the page are pushed
